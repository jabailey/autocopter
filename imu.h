/*
 MPU9250 code derived from MPU9250 Basic Example Code by Kris Winer
 date: April 1, 2014
 license: Beerware - Use this code however you'd like. If you
 find it useful you can buy me a beer some time.
 Modified by Brent Wilkins July 19, 2016
 Modified by James Bailey 2019

 SDA and SCL should have external pull-up resistors (to 3.3V).
 10k resistors are on the EMSENSR-9250 breakout board.

 Hardware setup:
 MPU9250 Breakout --------- Arduino
 VDD ---------------------- 3.3V
 VDDI --------------------- 3.3V
 SDA ----------------------- A4
 SCL ----------------------- A5
 GND ---------------------- GND
*/

#include <MPU9250.h>
#include <quaternionFilters.h>
#include <Wire.h>

#define IMU_SERIAL_DEBUG false

#define I2Cclock 400000
#define I2Cport Wire
#define MPU9250_ADDRESS MPU9250_ADDRESS_AD0

MPU9250 imu(MPU9250_ADDRESS, I2Cport, I2Cclock);

void setupImu() {
  Wire.begin();

  // Confirm that the IMU is found at the expected I2C address
  byte address = imu.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);
  if (address != 0x71) {
#if IMU_SERIAL_DEBUG
    Serial.print(F("Incorrect MPU 9250 Address (should be 0x71): 0x"));
    Serial.println(address, HEX);
    Serial.flush();
#endif
    abort();
  }
#if IMU_SERIAL_DEBUG
  Serial.println(F("MPU9250 is online..."));
#endif
  
  // Perform self test to check integrity of IMU
  imu.MPU9250SelfTest(imu.selfTest);
  // TODO: assert test success before proceeding?

#if IMU_SERIAL_DEBUG
  Serial.print(F("Self test acceleration trim % deviation from factory:\tx-axis = "));
  Serial.print(imu.selfTest[0], 1);
  Serial.print(F(";\ty-axis = "));
  Serial.print(imu.selfTest[1], 1);
  Serial.print(F(";\tz-axis = "));
  Serial.println(imu.selfTest[2], 1);
  Serial.print(F("Self test gyration trim % deviation from factory:\tx-axis = "));
  Serial.print(imu.selfTest[3], 1);
  Serial.print(F(";\ty-axis = "));
  Serial.print(imu.selfTest[4], 1);
  Serial.print(F(";\tz-axis = "));
  Serial.println(imu.selfTest[5], 1);
#endif

  // Calibrate gyro and accelerometers, load biases in bias registers
  imu.calibrateMPU9250(imu.gyroBias, imu.accelBias);
  
  // Initialize device for active mode read of acclerometer, gyroscope, and
  // temperature
  imu.initMPU9250();
#if IMU_SERIAL_DEBUG
  Serial.println(F("MPU9250 initialized for active data mode..."));
#endif
  
  // Confirm that the magnetometer is found at the expected I2C address
  address = imu.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
  if (address != 0x48) {
#if IMU_SERIAL_DEBUG
    Serial.print(F("Incorrect AK8963 Address (should be 0x48): 0x"));
    Serial.println(address, HEX);
    Serial.flush();
#endif
    abort();
  }
#if IMU_SERIAL_DEBUG
  Serial.println(F("AK8963 is online..."));
#endif
  
  // Get magnetometer factory calibration from AK8963 ROM
  // Initialize device for active mode read of magnetometer
  imu.initAK8963(imu.factoryMagCalibration);
#if IMU_SERIAL_DEBUG
  Serial.println(F("AK8963 initialized for active data mode..."));
  Serial.print(F("Factory sensitivity adjustments:\tx-axis = "));
  Serial.print(imu.factoryMagCalibration[0], 2);
  Serial.print(F(";\ty-axis = "));
  Serial.print(imu.factoryMagCalibration[1], 2);
  Serial.print(F(";\tz-axis = "));
  Serial.println(imu.factoryMagCalibration[2], 2);
#endif

  // Initialize sensor resolutions, only need to do this once
  imu.getAres();
  imu.getGres();
  imu.getMres();

  imu.yaw = 0;
  imu.pitch = 0;
  imu.roll = 0;
}

void loopImu() {
  // If intPin goes high, all data registers have new data
  // On interrupt, check if data ready interrupt
  if (imu.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01) {
    imu.readAccelData(imu.accelCount);  // Read the x/y/z adc values

    // Now we'll calculate the accleration value into actual g's
    // This depends on scale being set
    imu.ax = (float)imu.accelCount[0] * imu.aRes; // - imu.accelBias[0];
    imu.ay = (float)imu.accelCount[1] * imu.aRes; // - imu.accelBias[1];
    imu.az = (float)imu.accelCount[2] * imu.aRes; // - imu.accelBias[2];

    imu.readGyroData(imu.gyroCount);  // Read the x/y/z adc values

    // Calculate the gyro value into actual degrees per second
    // This depends on scale being set
    imu.gx = (float)imu.gyroCount[0] * imu.gRes;
    imu.gy = (float)imu.gyroCount[1] * imu.gRes;
    imu.gz = (float)imu.gyroCount[2] * imu.gRes;

    imu.readMagData(imu.magCount);  // Read the x/y/z adc values

    // Calculate the magnetometer values in milliGauss
    // Include factory calibration per data sheet and user environmental
    // corrections
    // Get actual magnetometer value, this depends on scale being set
    imu.mx = (float)imu.magCount[0] * imu.mRes
               * imu.factoryMagCalibration[0] - imu.magBias[0];
    imu.my = (float)imu.magCount[1] * imu.mRes
               * imu.factoryMagCalibration[1] - imu.magBias[1];
    imu.mz = (float)imu.magCount[2] * imu.mRes
               * imu.factoryMagCalibration[2] - imu.magBias[2];
  }

  // Must be called before updating quaternions!
  imu.updateTime();

  // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
  // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
  // (+ up) of accelerometer and gyro! We have to make some allowance for this
  // orientationmismatch in feeding the output to the quaternion filter. For the
  // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
  // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
  // modified to allow any convenient orientation convention. This is ok by
  // aircraft orientation standards! Pass gyro rate as rad/s
  MahonyQuaternionUpdate(
      imu.ax, imu.ay, imu.az,
      imu.gx * DEG_TO_RAD, imu.gy * DEG_TO_RAD, imu.gz * DEG_TO_RAD,
      imu.my, imu.mx, imu.mz, imu.deltat);

  imu.delt_t = millis() - imu.count;

  // Define output variables from updated quaternion---these are Tait-Bryan
  // angles, commonly used in aircraft orientation. In this coordinate system,
  // the positive z-axis is down toward Earth. Yaw is the angle between Sensor
  // x-axis and Earth magnetic North (or true North if corrected for local
  // declination, looking down on the sensor positive yaw is counterclockwise.
  // Pitch is angle between sensor x-axis and Earth ground plane, toward the
  // Earth is positive, up toward the sky is negative. Roll is angle between
  // sensor y-axis and Earth ground plane, y-axis up is positive roll. These
  // arise from the definition of the homogeneous rotation matrix constructed
  // from quaternions. Tait-Bryan angles as well as Euler angles are
  // non-commutative; that is, the get the correct orientation the rotations
  // must be applied in the correct order which for this configuration is yaw,
  // pitch, and then roll.
  // For more see
  // http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
  // which has additional links.
  imu.yaw   = atan2(2.0f * (*(getQ()+1) * *(getQ()+2) + *getQ()
                * *(getQ()+3)), *getQ() * *getQ() + *(getQ()+1)
                * *(getQ()+1) - *(getQ()+2) * *(getQ()+2) - *(getQ()+3)
                * *(getQ()+3));
  imu.pitch = -asin(2.0f * (*(getQ()+1) * *(getQ()+3) - *getQ()
                * *(getQ()+2)));
  imu.roll  = atan2(2.0f * (*getQ() * *(getQ()+1) + *(getQ()+2)
                * *(getQ()+3)), *getQ() * *getQ() - *(getQ()+1)
                * *(getQ()+1) - *(getQ()+2) * *(getQ()+2) + *(getQ()+3)
                * *(getQ()+3));
  imu.pitch *= RAD_TO_DEG;
  imu.yaw   *= RAD_TO_DEG;

  // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
  //    8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
  // - http://www.ngdc.noaa.gov/geomag-web/#declination
  imu.yaw  -= 8.5;
  imu.roll *= RAD_TO_DEG;

#if IMU_SERIAL_DEBUG
  Serial.print("Yaw, Pitch, Roll: ");
  Serial.print(imu.yaw, 2);
  Serial.print(", ");
  Serial.print(imu.pitch, 2);
  Serial.print(", ");
  Serial.println(imu.roll, 2);

  Serial.print("rate = ");
  Serial.print((float)imu.sumCount / imu.sum, 2);
  Serial.println(" Hz");
#endif

  imu.count = millis();
  imu.sumCount = 0;
  imu.sum = 0;
}
