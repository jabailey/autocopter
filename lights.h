/*
MIT License

Copyright (c) 2019 James Bailey

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define NAV_LIGHTS_PIN 13
#define AC_BEACON_PIN 12
#define STROBE_PIN 11

const unsigned long STROBE_INTERVAL = 1000;
const unsigned long STROBE_DURATION = 100;
unsigned long last_strobe_start;

enum BeaconState {
  ON,
  OFF
};

enum StrobeState {
  OFF_0,
  ON_0,
  OFF_1,
  ON_1,
  OFF_2
};

BeaconState beacon_state;
StrobeState strobe_state;

void setupLights() {
  pinMode(NAV_LIGHTS_PIN, OUTPUT);
  pinMode(AC_BEACON_PIN, OUTPUT);
  pinMode(STROBE_PIN, OUTPUT);

  digitalWrite(NAV_LIGHTS_PIN, LOW);
  digitalWrite(AC_BEACON_PIN, LOW);
  digitalWrite(STROBE_PIN, LOW);
}

void enableNavLights() {
  digitalWrite(NAV_LIGHTS_PIN, HIGH);
  digitalWrite(AC_BEACON_PIN, HIGH);
  last_strobe_start = millis();
}

void loopLights() {
  unsigned long delta_t = millis() - last_strobe_start;
  if (delta_t >= STROBE_INTERVAL) {
    last_strobe_start = millis();
    delta_t = 0;
  }
  switch (beacon_state) {
    case OFF:
    default:
      if (delta_t < STROBE_DURATION) {
        digitalWrite(AC_BEACON_PIN, HIGH);
        beacon_state = ON;
      }
      break;
    case ON:
      if (delta_t >= STROBE_DURATION) {
        digitalWrite(AC_BEACON_PIN, LOW);
        beacon_state = OFF;
      }
      break;
  }
  switch (strobe_state) {
    case OFF_0:
    default:
      if (delta_t > (STROBE_INTERVAL / 2) - STROBE_DURATION) {
        digitalWrite(STROBE_PIN, HIGH);
        strobe_state = ON_0;
      }
      break;
    case ON_0:
      if (delta_t > (STROBE_INTERVAL / 2)) {
        digitalWrite(STROBE_PIN, LOW);
        strobe_state = OFF_1;
      }
      break;
    case OFF_1:
      if (delta_t > (STROBE_INTERVAL / 2) + STROBE_DURATION) {
        digitalWrite(STROBE_PIN, HIGH);
        strobe_state = ON_1;
      }
      break;
    case ON_1:
      if (delta_t > (STROBE_INTERVAL / 2) + STROBE_DURATION * 2) {
        digitalWrite(STROBE_PIN, LOW);
        strobe_state = OFF_2;
      }
      break;
    case OFF_2:
      if (delta_t < (STROBE_INTERVAL / 2)) {
        strobe_state = OFF_0;
      }
      break;
  }
}
