template<typename T, unsigned int N>
class AvgFilter {
public:
  AvgFilter() : _num(0), _i(0) {}
  
  void update(const T& value) {
    if (_num == 0) {
      _num = 1;
      _average = value;
    }
    else if (_num < N) {
      _average = (_average * _num + value) / (++_num);
    }
    else {
      _average = (_average * N + value - _buffer[_i]) / N;
    }
    _buffer[_i] = value;
    _i = (_i + 1) % N;
  }

  const T& get() const {
    return _average;
  }

private:
  T _buffer[N];
  T _average;
  unsigned int _num;
  unsigned int _i;
};
