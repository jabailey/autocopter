/*
MIT License

Copyright (c) 2019 James Bailey

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define READ_THRO_AND_RUDD_PWM true

// These are fairly brittle and cannot be moved around arbitrarily, because
// there are limited interrupt capabilities available; this code depends on the
// PWM inputs belonging to specific input registers on the microcontroller.
#define THRO_PWM_PIN 8
#define RUDD_PWM_PIN 14
#define ELEV_PWM_PIN 2
#define AILE_PWM_PIN 3

#define THRO_PCMSK *digitalPinToPCMSK(THRO_PWM_PIN)
#define THRO_PCINT digitalPinToPCMSKbit(THRO_PWM_PIN)
#define THRO_PCIE  digitalPinToPCICRbit(THRO_PWM_PIN)
#define THRO_PCPIN *portInputRegister(digitalPinToPort(THRO_PWM_PIN))
#if (THRO_PCIE == 0)
#define THRO_PCINT_vect PCINT0_vect
#elif (THRO_PCIE == 1)
#define THRO_PCINT_vect PCINT1_vect
#else
#error THRO input must be on PCINT 0 or 1 (port B or C)
#endif

#define RUDD_PCMSK *digitalPinToPCMSK(RUDD_PWM_PIN)
#define RUDD_PCINT digitalPinToPCMSKbit(RUDD_PWM_PIN)
#define RUDD_PCIE  digitalPinToPCICRbit(RUDD_PWM_PIN)
#define RUDD_PCPIN *portInputRegister(digitalPinToPort(RUDD_PWM_PIN))
#if (RUDD_PCIE == 0)
#define RUDD_PCINT_vect PCINT0_vect
#elif (RUDD_PCIE == 1)
#define RUDD_PCINT_vect PCINT1_vect
#elif (RUDD_PCIE == 2)
#define RUDD_PCINT_vect PCINT1_vect
#else
#error RUDD input must be on PCINT 0 or 1 (port B or C)
#endif

#if (THRO_PCIE == RUDD_PCIE)
#error RUDD and THRO inputs must be on different ports
#endif
 
#define PWM_INT(F, N)\
volatile unsigned int width##N = 0;\
volatile unsigned long rise##N = 0;\
volatile byte last##N = LOW;\
F {\
  byte current = digitalRead(N##_PWM_PIN);\
  if (current != last##N) {\
    last##N = current;\
    if (current == HIGH) {\
      rise##N = micros();\
    }\
    else if(rise##N != 0) {\
      width##N = micros() - rise##N;\
    }\
  }\
}

PWM_INT(ISR(THRO_PCINT_vect), THRO)
PWM_INT(ISR(RUDD_PCINT_vect), RUDD)
PWM_INT(void intELEV(), ELEV)
PWM_INT(void intAILE(), AILE)

void setupPwmIn() {
  pinMode(ELEV_PWM_PIN, INPUT);
  pinMode(AILE_PWM_PIN, INPUT);
  // ELEV and AILE interrupts are easy, on the interrupt-enabled pins
  attachInterrupt(digitalPinToInterrupt(ELEV_PWM_PIN), intELEV, CHANGE);
  attachInterrupt(digitalPinToInterrupt(AILE_PWM_PIN), intAILE, CHANGE);

#if READ_THRO_AND_RUDD_PWM
  pinMode(THRO_PWM_PIN, INPUT);
  pinMode(RUDD_PWM_PIN, INPUT);
  // Enable interrupts for the THRO and RUDD PWM signals, which must live on
  // different ports in order for this to work.
  THRO_PCMSK |= (1 << THRO_PCINT);
  PCICR |= (1 << THRO_PCIE);
  RUDD_PCMSK |= (1 << RUDD_PCINT);
  PCICR |= (1 << RUDD_PCIE);
#endif
}
