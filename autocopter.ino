/*
MIT License

Copyright (c) 2019 James Bailey

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/* This sketch is for a microcontroller placed between the radio receiver and
 * the motors and servos on an RC helicopter, so that the microcontroller can
 * use an accelerometer to attempt to stabilize the vehicle so it flies more
 * like a quadcopter.
 * 
 * The plan is to read the PWM signals from the receiver, interpret those as
 * forward/back, left/right, up/down, and turn right/turn left commands, and
 * send those as setpoints through a series of nested PID controllers that
 * read the accelerometer and write out to the motors/servos.
 * 
 * The receiver sends out PWM signals on multiple channels, but it staggers
 * them in time so that they could be combined into a PPM signal
 * 
 * PWM signals:
 * 
 * THRO:  ______---_____________________________________---____________________
 * AILE:  __________--______________________________________--_________________
 * ELEV:  ______________----____________________________________--_____________
 * RUDD:  __________________---_____________________________________---________
 * 
 * PPM:   ______---_--__-------_________________________---_--__--__---________
 * 
 * This program could be structured to block while it reads the PWM signals and
 * then run the main code in the empty space between them, but the timing and
 * logic get really hairy. Ideally, we'd have an interrupt on each PWM input,
 * which is available on every digital input on fancier boards like the Teensy.
 * However, on most Arduino boards only digital inputs 2 and 3 can be directly
 * tied to interrupts.
 * 
 * But, there's a way out: inputs 2 and 3 belong to one input register, and the
 * remaining inputs are split across 2 additional input registers. Each input
 * register can have a single interrupt for changes on any of its pins, and the
 * interrupt can be masked to trigger for specific pins. There are fancy
 * libraries that allow you to exploit this to detect when any specific pin
 * changes, but we only need interrupts on 4 channels here, so we can put 2
 * channels onto pins 2 and 3, and the remaining 2 each on a pin in the other
 * two input registers, with those registers' interrupts set to only fire for
 * changes on the relevant pins.
 * 
 *
 * Pin reference:
 * 
 * Pin - ICR - Num - Func - PWM - Mode  - Use
 * D0  - 2   - 0   - TX   -     -       -
 * D1  - 2   - 1   - RX   -     -       -
 * D2  - 2   - 2   - INT0 -     - IN    - ELEV (pitch command) (pwm interrupt on D2)
 * D3  - 2   - 3   - INT1 - PWM - IN    - AILE (roll command) (pwm interrupt on D3)
 * D4  - 2   - 4   -      -     - IN    - IMU Zero
 * D5  - 2   - 5   -      - PWM - Servo - PITCH
 * D6  - 2   - 6   -      - PWM - Servo - ROLL
 * D7  - 2   - 7   -      -     -       - 
 * D8  - 0   - 8   -      -     - IN    - THRO (throttle command) (pwm interrupt on ICR 0)
 * D9  - 0   - 9   -      - PWM - Servo - LOWER_ROTOR
 * D10 - 0   - 10  -      - PWM - Servo - UPPER_ROTOR
 * D11 - 0   - 11  -      - PWM - OUT   - Strobe lights
 * D12 - 0   - 12  -      -     - OUT   - Anti-collision beacon lights
 * D13 - 0   - 13  - LED  -     - OUT   - Nav lights
 * A0  - 1   - 14  -      -     - IN    - RUDD (yaw rate command) (pwm interrupt on ICR 1)
 * A1  - 1   - 15  -      -     - A     - PITCH_TRIM
 * A2  - 1   - 16  -      -     - A     - ROLL_TRIM
 * A3  - 1   - 17  -      -     - A     - YAW_TRIM
 * A4  - 1   - 18  - SDA  -     - Wire  - Data (accelerometer)
 * A5  - 1   - 19  - SCL  -     - Wire  - I2C Clock (accelerometer)
 * A6  - 1   - 20  -      -     -       -
 * A7  - 1   - 21  -      -     -       -
 */

#define PID_SERIAL_PLOT true

#include "avg_filter.h"
#include "balance_pid.h"
#include "imu.h"
#include "lights.h"
#include "pwm_in.h"

#include <Servo.h>

#define PITCH_PIN       5
#define ROLL_PIN        6
#define LOWER_ROTOR_PIN 9
#define UPPER_ROTOR_PIN 10

#define IMU_ZERO_PIN 4
#define PITCH_TRIM_PIN 15
#define ROLL_TRIM_PIN  16
#define YAW_TRIM_PIN   17

double pitch_trim = 0;
double roll_trim = 0;
double yaw_trim = 0;

int pitch_servo_command = 0;
int roll_servo_command  = 0;

Servo pitch_servo;
Servo roll_servo;
Servo lower_rotor_servo;
Servo upper_rotor_servo;

// Variables for simulating the servo response
const int SERVO_D_PER_MS = 1; // simulated servo can swing 1000 degrees in 1 s
int pitch_servo_sim;
int roll_servo_sim;

const double MAX_TILT = 25.0;

const unsigned long MIN_W = 1000;
const unsigned long MAX_W = 2000;

double mapd(double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

double constrainToSafe(double in, double min, double max, double safe) {
  if (in < min || in > max) {
    return safe;
  }
  return in;
}

const unsigned long LOOP_PERIOD = 5;

AvgFilter<double, 20> filtered_pitch;
AvgFilter<double, 20> filtered_roll;

void setup() {
  // Setup lights first, to turn them off, so they only turn on once
  // setup is complete.
  setupLights();
  
  Serial.begin(38400);
  while(!Serial) {} // Wait for serial to be ready

  // Initialize the trim values - these are hardware trim pots that should allow
  // the "zero" position of the PID controllers to be tuned such that the
  // vehicle hovers in place when the inputs are neutral.
  pitch_trim = mapd((double)analogRead(PITCH_TRIM_PIN), 0.0, 1024.0, -MAX_TILT/2.0, MAX_TILT/2.0);
  roll_trim =  mapd((double)analogRead(ROLL_TRIM_PIN),  0.0, 1024.0, -MAX_TILT/2.0, MAX_TILT/2.0);
  yaw_trim = analogRead(YAW_TRIM_PIN);

  pitch_servo.attach(PITCH_PIN);
  roll_servo .attach(ROLL_PIN);
  lower_rotor_servo.attach(LOWER_ROTOR_PIN);
  upper_rotor_servo.attach(UPPER_ROTOR_PIN);

  pinMode(IMU_ZERO_PIN, INPUT_PULLUP);

  setupBalancePid(LOOP_PERIOD);
  setupImu();
  setupPwmIn();

  enableNavLights();
}

void loop() {
  static unsigned long last_run = 0;

  unsigned long elapsed = millis() - last_run;
  if (elapsed < LOOP_PERIOD) {
    delay(LOOP_PERIOD - elapsed);
  }
  last_run = millis();

  if(digitalRead(IMU_ZERO_PIN) == LOW) {
    pitch_trim = -imu.pitch;
    roll_trim = -imu.roll;
  }

  loopImu();
  loopLights();

  // Map PWM pulse widths to desired pitch and roll values within our allowable
  // control range, adding the trim position to the final result for manual tuning.
  pitch_set = constrainToSafe((double)widthELEV, (double)MIN_W, (double)MAX_W, (double)(MIN_W + MAX_W) / 2.0);
  pitch_set = mapd(pitch_set, (double)MIN_W, (double)MAX_W, -MAX_TILT, MAX_TILT);
  roll_set  = constrainToSafe((double)widthAILE, (double)MIN_W, (double)MAX_W, (double)(MIN_W + MAX_W) / 2.0);
  roll_set =  mapd(roll_set,  (double)MIN_W, (double)MAX_W, -MAX_TILT, MAX_TILT);

  filtered_pitch.update(imu.pitch);
  pitch_in = filtered_pitch.get() + pitch_trim;
  filtered_roll.update(imu.roll);
  roll_in = filtered_roll.get() + roll_trim;

  // Compute, constrain, and send servo commands
  loopBalancePid();
  pitch_servo_command = constrain((int)round(pitch_out), 0, 180);
  roll_servo_command =  constrain((int)round(roll_out), 0, 180);
  pitch_servo.write(pitch_servo_command);
  roll_servo .write(roll_servo_command);

  // Simulate servo response
  // TODO - something fishy here!
  if (pitch_servo_sim != pitch_servo_command) {
    int error = pitch_servo_command - pitch_servo_sim;
    if (abs(error) < SERVO_D_PER_MS * LOOP_PERIOD) {
      pitch_servo_sim = pitch_servo_command;
    }
    else {
      pitch_servo_sim += (error > 0 ? 1 : -1) * SERVO_D_PER_MS * LOOP_PERIOD;
    }
  }
  if (roll_servo_sim != roll_servo_command) {
    int error = roll_servo_command - roll_servo_sim;
    if (abs(error) < SERVO_D_PER_MS * LOOP_PERIOD) {
      roll_servo_sim = roll_servo_command;
    }
    else {
      roll_servo_sim += (error > 0 ? 1 : -1) * SERVO_D_PER_MS * LOOP_PERIOD;
    }
  }
/*
  // Simulate pitch and roll from servo position using simple proportional-on-error control
  double pitch_target = mapd((double)pitch_servo_sim, 0.0, 180.0, -MAX_TILT, MAX_TILT);
  double roll_target  = mapd((double)roll_servo_sim,  0.0, 180.0, -MAX_TILT, MAX_TILT);
  double pitch_error = pitch_in - pitch_target;
  double roll_error  = roll_in - roll_target;
  // set kp to 1 to tie pitch and roll directly to the servos
  static const double SIM_KP = 1.0;
  pitch_in -= SIM_KP * pitch_error;
  roll_in -=  SIM_KP * roll_error;
  */
  
#if PID_SERIAL_PLOT
  static unsigned long last_serial = 0;
  if (millis() - last_serial > 100) {
    // Pitch PID
    ///*
    Serial.print(pitch_set, 3);
    Serial.print(" ");
    Serial.print(pitch_in, 3);
    Serial.print(" ");
    Serial.print((pitch_out - 90.0) / (90.0 / MAX_TILT), 3);
    Serial.print(" ");
    //*/

    // RollPID
    ///*
    Serial.print(roll_set, 3);
    Serial.print(" ");
    Serial.print(roll_in, 3);
    Serial.print(" ");
    Serial.print((roll_out - 90.0) / (90.0 / MAX_TILT), 3);
    //Serial.print(" ");
    //*/

    // PWM inputs
    /*
    Serial.print(widthRUDD);
    Serial.print(" ");
    Serial.print(widthTHRO);
    Serial.print(" ");
    Serial.print(widthELEV);
    Serial.print(" ");
    Serial.print(widthAILE);
    */
 
    Serial.println();
    last_serial = millis();
  }
#endif
  
  // TODO (future): mode to bypass control loops and forward the signals as-is.
  // TODO (future): blinky lights!
}
