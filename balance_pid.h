/*
MIT License

Copyright (c) 2019 James Bailey

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <PID_v1.h>

double pitch_in = 0;
double roll_in  = 0;

double pitch_out = 0;
double roll_out  = 0;

double pitch_set = 0;
double roll_set  = 0;

const double KP = 2.0;
const double KI = 8.0;
const double KD = 0.2;

PID pitch_pid(&pitch_in, &pitch_out, &pitch_set, KP, KI, KD, DIRECT, P_ON_M);
PID roll_pid (&roll_in,  &roll_out,  &roll_set,  KP, KI, KD, DIRECT);//, P_ON_M);

void setupBalancePid(long sample_time) {
  pitch_pid.SetMode(AUTOMATIC);
  roll_pid .SetMode(AUTOMATIC);
  pitch_pid.SetSampleTime(sample_time);
  roll_pid. SetSampleTime(sample_time);
  pitch_pid.SetOutputLimits(15.0, 165.0);
  roll_pid .SetOutputLimits(15.0, 165.0);
}

void loopBalancePid() {
  pitch_pid.Compute();
  roll_pid.Compute();
}
